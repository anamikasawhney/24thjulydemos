﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceDemo
{
    internal class Employee1
    {
        int id;
        public int Id
        {
            get { return id; }
            set { if (value < 0) id = 0; else  id = value; }
        }

        string name;
        public string Name
        {
            get
            {
                 return name;
            }
            set // also used for validation
            {  if(value.Length <3)
                name = string.Empty;
            else 
                    name = value;
            }
        }
         
    }
}
