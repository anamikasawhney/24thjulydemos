﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceDemo
{
    internal class PartTimeEmployee : Employee
    {
        int contractDuration;
        float chargesPerDay;
        float totalCharges;

        public PartTimeEmployee() : base() { }
        public PartTimeEmployee(int id, string name, string dept, 
            string manager,int contractDuration,
        float chargesPerDay) 
            : base(id, name, dept, manager)
        {
            this.contractDuration = contractDuration;
            this.chargesPerDay = chargesPerDay;
                    }
        //public void GetPartTimeDetails()
        public  override void GetDetails()
        {
            base.GetDetails();
            Console.WriteLine("Enter contractDuration");
            contractDuration = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter chargesPerDay");
            chargesPerDay = float.Parse(Console.ReadLine());

        }
       public float GetTotalCharges()
        {
            totalCharges = contractDuration * chargesPerDay;
            return  totalCharges;
            
        }

        //public void DispalyPartTimeDetails()
        public override void DispalyDetails()
        {
            base.DispalyDetails();
            Console.WriteLine("contractDuration is " + contractDuration);
            Console.WriteLine("chargesPerDay is " + chargesPerDay);
            Console.WriteLine("totalCharges is " + totalCharges);

        }

    }
}
