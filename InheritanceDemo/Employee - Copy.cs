﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace InheritanceDemo
{
    internal class Employee
    {
        int id;
        string name;
        string dept;
        string manager;
        public Employee()
        {

        }
        public Employee(int id, string name, string dept, string manager)
        {
            this.id = id;
            this.name = name;
            this.dept = dept;
            this.manager = manager;
               
            
            
        }
        public virtual void GetDetails()
        {
            Console.WriteLine("Enter Id");
            id = int.Parse(Console.ReadLine());
            Console.WriteLine("Enter NAme");
            name = Console.ReadLine();
            Console.WriteLine("Enter Manager");
            manager = Console.ReadLine();
            Console.WriteLine("ENter Dept");
            dept = Console.ReadLine();
        }
        public virtual void DispalyDetails()
        {
            Console.WriteLine("ID is " +     id);
            Console.WriteLine("Name is "  + name);
            Console.WriteLine("Dept is " + dept);
            Console.WriteLine("Manger is " + manager);

        }



    }
}
