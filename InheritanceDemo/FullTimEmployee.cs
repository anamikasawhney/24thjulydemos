﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace InheritanceDemo
{
    internal class FullTimeEmployee : Employee
    {
        DateTime joiningDate;
        float salary;
        // DEF constr in child class
        public FullTimeEmployee() : base()
        { }
        // Para const in child class
        public FullTimeEmployee(int id, string name, string dept, string manager,
            DateTime joiningDate, float salary)
            : base (id, name, dept, manager)
        {
            this.joiningDate = joiningDate;
            this.salary = salary;

        }

        //public void GetFullTimeDetails()
        // METHOD OEVRRIDING
        // METHOD OEVRRIDING > Redefining function or base class 
        // in child class
        public override void GetDetails()
        {
            base.GetDetails();
            Console.WriteLine("Enter salary");
            salary = float.Parse(Console.ReadLine());
            Console.WriteLine("Enter joiningDate");
            joiningDate = DateTime.Parse(Console.ReadLine());

        }
        //public void DispalyFullTimeDetails()
        public override void DispalyDetails()
        {
            base.DispalyDetails();
            Console.WriteLine("joiningDate is " + joiningDate);
            Console.WriteLine("salary is " + salary);
           
        }

    }
}
