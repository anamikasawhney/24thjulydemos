﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using static System.Formats.Asn1.AsnWriter;

namespace InheritanceDemo
{
    internal class Student
    {
        int rn;
        string name;
        public void Get()
        {
            Console.WriteLine("Enter Rn");
            rn = byte.Parse(Console.ReadLine());
            Console.WriteLine("Enter Name");
            name = Console.ReadLine();
        }
        public void Display()
        {
            Console.WriteLine("Rn is " + rn);
            Console.WriteLine("Name is " + name);
        }
    }
    class Sports : Student
    {
        string sportsName;
        protected int score;
        public void Get()
        {
            base.Get();
            Console.WriteLine("Enter sportsName");
            sportsName = Console.ReadLine();
            Console.WriteLine("Enter score");
            score = Byte.Parse(Console.ReadLine());
        }
        public void Display()
        {
            base.Display();
            Console.WriteLine("sportsName is " + sportsName);
            Console.WriteLine("score is " + score);
        }
    }
    class Test : Sports
    {
        int[] marks = new int[5];
        protected int totalMarks;
        public void Get()
        {
            base.Get();
            Console.WriteLine("Enter Marks");
            for (int i = 0; i < 5; i++)
                marks[i] = Byte.Parse(Console.ReadLine());
        }
        public void CalulateTotalMarks()
        {
            foreach (int num in marks)
                totalMarks += num;
        }
        public void Display()
        {
            base.Display();
            Console.WriteLine("totalMarks are " + totalMarks);

        }
    }
    class Result : Test
    {
        int totalScore;
        public void CalulateTotalScore()
        {
            base.CalulateTotalMarks();
            totalScore = score + totalMarks;

        }
        public void Display()
        {
            base.Display();
            Console.WriteLine("totalMarks are " + totalScore);

        }
    }
}
