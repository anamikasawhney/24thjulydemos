﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractClassInterface
{
    abstract internal class Figure
    {
        protected  int x, y, area;
        //public Figure()
        //{

        //}
        //public Figure(int x, int y, int area)
        //{
        //     this.x = x;
        //    this.y = y;
        //    this.area = area;
        //}
        abstract public void GetDimensions();
        abstract public void CalculateArea();
        public void DisplayArea()
        {
            Console.WriteLine("Area is " + area);
        }
    }

    class Square : Figure
    {
        //public Square() : base()
        //{

        //}
        //public Square(int x, int y, int area) : base (x,y,area)
        //{


        //}
        public override void CalculateArea()
        {
            area = x * x;
        }

        public override void GetDimensions()
        {
            Console.WriteLine("Enter Side");
            x = Byte.Parse(Console.ReadLine());

        }
    }
    class Rectangle : Figure
    {
        public override void CalculateArea()
        {
            area = x * y;
        }

        public override void GetDimensions()
        {
            Console.WriteLine("Enter length");
            x = Byte.Parse(Console.ReadLine());
            Console.WriteLine("Enter breadth");
            y = Byte.Parse(Console.ReadLine());
        }
    }


}
