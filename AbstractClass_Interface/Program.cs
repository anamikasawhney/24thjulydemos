﻿namespace AbstractClassInterface
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Square square = new Square();
            square.GetDimensions();
            square.CalculateArea();
            square.DisplayArea();
        }
    }
}