﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractClass_Interface
{
    internal interface IFigure
    {
        public void GetDimensions();
        public void CalculateArea();
        public void DisplayArea();
    }
    // interfaces are implemeted by classes
    class Sqaure1 : IFigure
    {
        int side, area;
        public void CalculateArea()
        {
            throw new NotImplementedException();
        }

        public void DisplayArea()
        {
            throw new NotImplementedException();
        }

        public void GetDimensions()
        {
            throw new NotImplementedException();
        }
    }

    class Rectangle1 : IFigure
    {
        public void CalculateArea()
        {
            throw new NotImplementedException();
        }

        public void DisplayArea()
        {
            throw new NotImplementedException();
        }

        public void GetDimensions()
        {
            throw new NotImplementedException();
        }
    }

    // interfaces can be used to implement multiple inheritance

    interface I1
    {
        public void GetA();
        public void GetB();

    }

    interface I2
    {
        public void GetC();
        public void GetD();
        public  void GetA();

    }

    // Multiple Inheritance
    class A : I1, I2
    {
        public void GetA()
        {
            throw new NotImplementedException();
        }

        public void GetB()
        {
            throw new NotImplementedException();
        }

        public void GetC()
        {
            throw new NotImplementedException();
        }

        public void GetD()
        {
            throw new NotImplementedException();
        }
    }

    class B : I1, I2
    {
        void I1.GetA()
        {
            throw new NotImplementedException();
        }

        void I2.GetA()
        {
            throw new NotImplementedException();
        }

        void I1.GetB()
        {
            throw new NotImplementedException();
        }

        void I2.GetC()
        {
            throw new NotImplementedException();
        }

        void I2.GetD()
        {
            throw new NotImplementedException();
        }
    }


}
