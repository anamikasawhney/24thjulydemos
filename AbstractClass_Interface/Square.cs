﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AbstractClass_Interface
{
    internal class Square
    {
        int side, area;
        public void GetDimensions()
        {
            Console.WriteLine("Enter Side");
            side = Byte.Parse(Console.ReadLine());
        }
        public void CalculateArea()
        {
            area = side * side;
        }
        public void DisplayArea()
        {
            Console.WriteLine("Area is " + area);
        }
    }
}
