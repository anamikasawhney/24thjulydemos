﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConstructorsDemo
{
    internal class Student
    {
        // System.Byte > byte
        // int > System.Int32 , Int32
        // System.String  >> string
        byte rn;
        string name;
        static string batch="DotNetFSD";
        Byte marks; // 0 -255 - 3200 > +32000
        string address;
        public void Get()
        {
            Console.WriteLine("Enter Rn");
            //rn =  Convert.ToByte(Console.ReadLine());
            rn = Byte.Parse(Console.ReadLine());
            Console.WriteLine("Enter Name");
            name = Console.ReadLine();
            //Console.WriteLine("Enter Batch");
            //batch = Console.ReadLine();
            Console.WriteLine("Enter Marks");
            marks = Byte.Parse(Console.ReadLine());
            Console.WriteLine("ENter Address");
            address = Console.ReadLine();

            //rn.ToString()
        }
        public void Display()
        {
            Console.WriteLine($"Rn is {rn}");
            Console.WriteLine($"Name is {name}");
            Console.WriteLine($"Address is {address}");
            Console.WriteLine($"Marks are {marks}");
            //Console.WriteLine($"Batch is {batch}");
        }
        public static void BatchDetails()
        {
            Console.WriteLine($"Batch Name is {batch}");

        }
        // Static Constructor  are used to initialize static variables
        // There can be only 1 SC
        // This cons shud be parameterless
        // It shud not have any access specifier
        static Student()
        {
            batch = "DotNetFSD";
             
        }
        
        // def cons
        public Student()
        {

        }
        // para cons
        public Student(byte rn)
        {// this means current object
            this.rn = rn;
            Console.WriteLine("Enter Name");
            name = Console.ReadLine();
            //Console.WriteLine("Enter Batch");
            //batch = Console.ReadLine();
            Console.WriteLine("Enter Marks");
            marks = Byte.Parse(Console.ReadLine());
            Console.WriteLine("ENter Address");
            address = Console.ReadLine();

        }
        // para cons
        public Student(byte rn, string name)
        {
            this.rn = rn;
            this.name = name;
            // Console.WriteLine("Enter Batch");
            //batch = Console.ReadLine();
            Console.WriteLine("Enter Marks");
            marks = Byte.Parse(Console.ReadLine());
            Console.WriteLine("ENter Address");
            address = Console.ReadLine();

        }
        // para cons
        public Student(byte rn, string name, string address)
        {
            this.rn = rn;
            this.name = name;
            this.address = address;
             address = Console.ReadLine();
            Console.WriteLine("Enter Marks");
            marks = Byte.Parse(Console.ReadLine());
            //Console.WriteLine("ENter batch");
            //batch = Console.ReadLine();

        }

        // fully para const
        public Student(byte rn, string name, string address, byte marks)
        {
            this.rn = rn;
            this.name = name;
            this.address = address;
            //this.batch = batch;
            this.marks = marks;
                 
        }

        public Student(Student student)
        {
            Console.WriteLine("Enter Rn");
            rn= Byte.Parse(Console.ReadLine());
            this.name = student.name;
            this.marks = student.marks;
            this.address= student.address;

        }
    }
}
