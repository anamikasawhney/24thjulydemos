﻿namespace ConstructorsDemo
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int x , y;

            ////Console.WriteLine("Enter characer");
            ////int x =  Console.Read();
            ////Console.WriteLine(x);
            //string y = Console.ReadLine();
            //char [] st = y.ToCharArray();
            //foreach(char c in st)
            //{
            //    Console.WriteLine(Convert.ToByte(c));
            //}

            // new keyword will do 2 things
            // 1. It allocates memory
            // 2. It calls constructor to initializec variables of object

            Student.BatchDetails();

            // This will invoke default constr  
            Student student = new Student();
            student.Get();
            student.Display();
            //This will invoke para constr
            Student student1 = new Student(3);
            student1.Display();

           
           
          
            //This will invoke para constr
            Student student2 = new Student(4, "Deepak");
            student2.Display();

            //This will invoke para constr
            Student student3 = new Student(10, "Ajay", "Delhi");
            student3.Display();


            Student student4 = new Student(11, "Ajay", "Delhi", 90);
            student4.Display();
            // call copy const, when you want to copy values of one object to other
            Student student5 = new Student(student4);
            student5.Display();

        }
    }
}