﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LINQ_MISC
{
    internal class Linq_Demo
    {
        static void Main()
        {
            List<int> numbers = new List<int>() { 1,2,3,4};
            // select * from employee;
            // Query Syntax to use LINQ
            var list = (from temp in numbers
                        select temp);
            foreach (var item in list)
            {
                Console.WriteLine(item);
            }
            // Print Max number
            int max = (from temp in numbers
                       select temp).Max();
            Console.WriteLine("MAX " + max);
            // Print Min number
            int min = (from temp in numbers
                       select temp).Min();

            Console.WriteLine("MIN " + min);
            int sum= (from temp in numbers
                      select temp).Sum();
            Console.WriteLine("SUM " + sum);

            double avg = (from temp in numbers
                         select temp).Average();

            var evennums = (from temp in numbers
                            where temp % 2 == 0
                            select temp);
            Console.WriteLine("Even numbers");
            foreach (var item in evennums)
            {
                Console.WriteLine(item);
            }

            // METHOD SYNTAX

            //var list1 = (from temp in numbers
            //             select temp);
            var list1 = numbers.ToList();
            Console.WriteLine("List ");
            foreach (var item in list1)
            {
                Console.WriteLine(item);
            }

            max = numbers.ToList().Max();
            sum = numbers.ToList().Sum();
            // Lambda Expression   x > x
            evennums = numbers.Where(x=>x % 2 == 0).ToList();
            string[] names = new string[]
            {
                "ajay sood",
                "deepak kumar",
                "pawan kumar",
                "mohan lal"
            };
            List<string> nameslist = names.ToList();
            Console.WriteLine("NAmes are ");
            foreach (var item in nameslist)
            {
                Console.WriteLine(item);
            }

            var listWhichContainKumar = (from temp in names
                                         where temp.Contains("kumar")
                                         select temp).ToList();

            Console.WriteLine("listWhichContainKumar");

            foreach (var item in listWhichContainKumar)
            {
                Console.WriteLine(item);
            }



        }
    }
}
