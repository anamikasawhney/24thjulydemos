﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo_24thJuly
{
    internal class ParamArray
    {
        static void Main()
        {
            Console.WriteLine("Sum is " + Add(1,2,3,4));
            Console.WriteLine("Sum is " + Add(1,2));
            Console.WriteLine("Sum is " + Add(1,2,3,4,5,6,7,8,99,10));
            Console.WriteLine(Add("aa","bb"));
            
        }
        // Object is the base type of all Data types

        static object Add(params object[] num)
        {
            int sum = 0;
            string str = "";
            foreach (var x in num)
            {
                if (x.GetType() == typeof(int))
                    sum += (int)x;

                return sum;
            }
            foreach (var x in num)
            {
                if (x.GetType() == typeof(string))
                    str += (string)x;

                return str;
            }

            return null;
        }
        

    }
}
