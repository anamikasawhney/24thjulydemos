﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo_24thJuly
{
    internal class DefaultParameters
    {
        static void Main()
        {
            Console.WriteLine("SI is " + SI(100000, 5, 10));
            Console.WriteLine("SI is " + SI(120000, 7));
            Console.WriteLine("SI is " + SI(122222));
            Console.WriteLine("SI is " + SI(122222, 8,8));
            



        }

        static float SI(float principal, float rate=9, int time=5)
        {
             return (principal * rate * time)/100;
        }
    }
}
