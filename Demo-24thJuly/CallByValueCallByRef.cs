﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo_24thJuly
{
    internal class CallByValueCallByRef
    {
        static void Main()
        {
            string name = "Old Name";
            Console.WriteLine("Name in Main before calling Change1 " + name);
            Change1(name); // Call By value
            Console.WriteLine("Name in Main after calling Change1 " + name);
            Change2(ref name);
            Console.WriteLine("Name in Main after calling Change2 " + name);

        }
        static void Change1(string name)
        {
            name = "New Name";
            Console.WriteLine("Name in change 1 is " + name);
        }
        static void Change2(ref string name)
        {
            name = "New Name";
            Console.WriteLine("Name in change 2 is " + name);
        }
    }
}
