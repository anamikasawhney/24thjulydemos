﻿namespace Demo_24thJuly
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Add some numbers
            Console.WriteLine($"Sum is {Add(10, 20)}");
            Console.WriteLine($"Sum is {Add1(10, 20, 30)}");
            Console.WriteLine($"Sum is {Add2(10, 20, 30, 30)}");
            Console.WriteLine($"Sum is {Add3(10, 20.8f)}");

        }
        static int Add(int x, int y)
        {
            return x + y;
        }
        static int Add1(int x, int y, int z)
        {
            return x + y + z;
        }
        static int Add2(int x, int y, int z, int a)
        {
            return x + y + z+a;
        }
        static float Add3(int x, float y)
            { return x + y; }
            
        static string Add4(string x, string y)
        {
            return string.Concat(x, ' ', y);
        }

    }
}