﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo_24thJuly
{
    internal class MethodOverloading
    {
        static void Main()
        {
            Console.WriteLine(Add(1,2,3,4));
            Console.WriteLine(Add("Ajay", "Sood"));
        }
        static int Add(int x, int y)
        {
            return x + y;
        }
        //static void Add(int x, int y)
        //{
          
        //}
        static int Add(int x, int y, int z)
        {
            return x + y + z;
        }

        static int Add(int x, int y, int z, int a)
        {
            return x + y+ z+a;
        }
        static string Add(string x, string y)
        {
            return x + ' ' + y;
        }


    }
}
