﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo_24thJuly
{
    internal class OutputParameters
    {
        static void Main()
        {
            int sum, differnce, product;
            float remainder;
            Calculation(1, 2, out sum, out differnce, out product, out remainder);
            Console.WriteLine($"Sum is {sum} Diference is {differnce} Product is" +
                $"{product} Remainder is {remainder}");
        }
        
        static void Calculation(int a, int b,
            out int sum, out int difference,
            out int product, out float remainder)
        {
            sum = a + b;
            difference = b - a;
            product = a * b;
            remainder = a % b;
        }


    }
}
