﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Demo_24thJuly
{
    internal class NamedParameter
    {
        static void Main()
        { 
            GetDetails(89898100, "ajay", "delhi", 9000, "deepak");
            // Named Parameter
            GetDetails(id: 900, managerName: "deepak", name: "ajay",
                address: "delhi", salary: 90000);

        }
        static void GetDetails(int id, string name, string address,
            int salary, string managerName)
        
        {
            Console.WriteLine($"Your Details are {name} \t {address} \t {salary} \t {managerName}");
        }

    }
}
